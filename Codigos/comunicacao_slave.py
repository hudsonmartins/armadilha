 #-*- coding: utf-8 -*-
import numpy as np
import RPi.GPIO as GPIO
import datetime, cv2, os, time, picamera, serial, sys, os.path, conta_moscas, config_xbee, gerenciador_pacotes
from time import sleep

BAUDRATE, SERIALPORT, ADDRESS = config_xbee.get_xbee_configuration()
MASTER_ADDRESS = config_xbee.get_master_address()
NAK = '000' #Dados para o pacote NAK (Not Acknowledge)
ACK = '111' #Dados para o pacote ACK (Acknowledge)
funcao = {'hello':'0', 'dados':'1', 'ack':'2'}
BROADCAST = '11111111'
MAX_PACOTES = 20
lista_pacotes = []

def receber_dados():
    """
       Recebe cabecalho e dados e trata de acordo com o que recebeu
       Retorna TRUE ao receber um pacote contendo ACK
       Envia o pacote para os outros pontos caso seja especificado para outro endereco	
    """
    cabecalho, dados = gerenciador_pacotes.recebe_pacote()
            
    if(cabecalho):	
	    bt_funcao, end_destino, end_remetente, checksum, id_pacote = gerenciador_pacotes.campos_cabecalho(cabecalho)
			    
	    #se o pacote recebido for direcionado para este ponto ou por BROADCAST
            if(end_destino == ADDRESS) or (end_destino == BROADCAST):
		    
		    if (bt_funcao == funcao.get('hello')):
				print "Pacote de Hello"
				if(lista_pacotes):
					#Se ainda nao enviou este pacote para a rede
					if(id_pacote not in lista_pacotes):				
						print "Enviando para rede mesh: ", dados	
						#Envia o mesmo pacote para a rede
						gerenciador_pacotes.enviar_dados(dados, 'hello', end_destino, end_remetente) 
						salva_id_pacote(id_pacote)

		    elif (bt_funcao == funcao.get('dados')):
				print "Pacote de dados"
			        if dados:
					print dados

	    	    elif (bt_funcao == funcao.get('ack')):	
				print "Pacote de ack"    
	    		        if (dados==NAK):#se  pacote for incoerente retorna falso
					print ('NAK')
					return False
	
				elif (dados==ACK):#se pacote for coerente retorna verdadeiro
					print('ACK')
					return True
    	    else:
		    #Se ainda nao enviou este pacote para a rede
		    if(id_pacote not in lista_pacotes):				
			print "Enviando para rede mesh: ", dados	
			#Envia o mesmo pacote para a rede
			gerenciador_pacotes.enviar_dados(dados, 'dados', end_destino, end_remetente) 
			salva_id_pacote(id_pacote)
				
    return False

def salva_id_pacote(id_pacote):
	'''
		Salva o ID do pacote em uma lista. Caso a lista tenha atingido o tamanho maximo remove o ultimo ID
	'''
	if(len(lista_pacotes) == MAX_PACOTES):
		del(lista_pacotes[MAX_PACOTES-1])
		lista_pacotes.append(id_pacote)
		#print "Lista de pacotes: ", lista_pacotes
	else:
		lista_pacotes.append(id_pacote)
		#print "Lista de pacotes: ", lista_pacotes

def envia_num_moscas():
	#contagem_moscas = conta_moscas.get_moscas() #Recebe a quantidade de moscas calculada pela rede neural
	contagem_moscas = [2]
	for num_moscas in contagem_moscas:
		dado = str(num_moscas)
		print "Enviando: ", dado
		start=time.time()	
		gerenciador_pacotes.enviar_dados(dado, 'dados', MASTER_ADDRESS, ADDRESS) #Envia a quantidade de moscas para o master

		recebe_ack = receber_dados()

		while(not recebe_ack): #enquanto nao receber ack
			
			if (time.time() > start+2): #envia novamente a cada 2 segundos se nao receber pacote coerente
					#print time.time()
					gerenciador_pacotes.enviar_dados(dado, 'dados', MASTER_ADDRESS, ADDRESS)
					print "Enviando: ", dado
					start=time.time()
			time.sleep(0.1)
			recebe_ack = receber_dados()

def comunica():
	
	start=time.time()	
	while(True):
		if (time.time() > start+30):
			envia_num_moscas()
			start=time.time()	
		receber_dados()

#comunica()	

