/*
*********************************************************************
Name: MySQL Database Creator
Version 1.0
*********************************************************************
*/

CREATE DATABASE `armadilha`;

USE `armadilha`;

CREATE TABLE `fazenda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `endereco` VARCHAR(150) NOT NULL,
  `moscas` INT NOT NULL,
  `data` DATE NOT NULL,
  `hora` TIME NOT NULL,
  PRIMARY KEY (`id`)
);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
