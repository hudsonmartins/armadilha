# -*- coding: utf-8 -*-
import sys, os, query_data
import numpy as np
from PyQt4 import QtGui
from PyQt4.QtCore import Qt
from PyQt4.QtGui import QApplication, QCursor
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure


class MainWindow(QtGui.QDialog):

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        # a figure instance to plot on
        self.figure = Figure()

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)

        self.plot()
	

	self.button_init_comm = QtGui.QPushButton(u"Iniciar Comunicação", self)
        self.button_init_comm.setFixedWidth(200)
        self.button_init_comm.clicked.connect(self.iniciar_comunicacao)

	# set the layout
        layout = QtGui.QVBoxLayout()
	layout.addStretch(1)
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)
	layout.addWidget(self.button_init_comm)
        self.setLayout(layout)
	
    def iniciar_comunicacao(self):
	print "comunicacao"

    def isCircle(self, xdata, ydata):
        if None not in (xdata, ydata):
            for i in range(len(dados)):
                if abs(xdata-x[i]) < 0.03 and abs(ydata-y[i]) < 0.03:
                    return True, i
        return False, 0

    def getInfo(self, xdata, ydata):
        is_circle, i = self.isCircle(xdata, ydata)
        if is_circle:

                id = dados[i][0]
                #print "id: ", id
                endereco = dados[i][1]
                #print "endereco: ", endereco
                data = dados[i][2]
                #print "data: ", data
                num_moscas = dados[i][3]
                #print "Numero de moscas: ", num_moscas
                return [id, endereco, data, num_moscas]
        return None

    def onMousePress(self, event):
        info = self.getInfo(event.xdata, event.ydata)
        if info:
            dialog = QtGui.QDialog()
            dialog.setGeometry(300, 300, 400, 400)
            info_label = QtGui.QLabel('id: '+str(info[0])+\
                  '\nQtd. moscas: '+ str(info[3])+\
                  '\nEndereco: ' + str(info[1])+\
                  '\nData: '+ str(info[2].strftime('%d/%m/%Y')), dialog)
            info_label.move(150, 150)
            dialog.exec_()

        """"
        print 'id: '+str(info[0])+\
              '\nQtd. moscas: '+ str(info[3])+\
              '\nEndereco: ' + str(info[1])+\
              '\nData: '+ str(info[2].strftime('%d/%m/%Y'))
        """

    def info_to_tip(self, info):
        return 'id: '+str(info[0])+\
                 '\nQtd. moscas: '+ str(info[3])

    def onMouseMotion(self, event, text):
        if None not in (event.xdata, event.ydata):
            is_circle, i = self.isCircle(event.xdata, event.ydata)
            if is_circle:
                QApplication.setOverrideCursor(QCursor(Qt.PointingHandCursor))
                text.set_text(self.info_to_tip(self.getInfo(event.xdata, event.ydata)))
                text.set_position([event.xdata, event.ydata])
                self.figure.canvas.draw()

            else:
                text.set_text('')
                self.figure.canvas.draw()
                QApplication.restoreOverrideCursor()

    #plot the graph
    def plot(self):

        global x, y, dados
        dados = query_data.query_all_data()

        N = len(dados)

        x = np.random.rand(N)
        y = np.random.rand(N)

        num_moscas = []
        maximo_moscas = 0.0

        for n in range(N):
            num_moscas.append(dados[n][3])
            if dados[n][3] > maximo_moscas:
                maximo_moscas = dados[n][3]

        area = np.pi * (15)**2

        ax = self.figure.add_subplot(111, picker=True)
        ax.set_xticks([])
        ax.set_yticks([])


        for i in range(N):
            if num_moscas[i]>0:
                ax.scatter(x[i], y[i], s=area, c='r', alpha=(num_moscas[i]*1.0/maximo_moscas), picker=True)

            else:
                ax.scatter(x[i], y[i], s=area, c='g', alpha=0.5, picker=True)

        text = ax.text(0, 0, '',
        bbox={'facecolor':'yellow', 'alpha':0.8})

        self.figure.canvas.mpl_connect('button_press_event', self.onMousePress)
        self.figure.canvas.mpl_connect('motion_notify_event', lambda event: self.onMouseMotion(event, text))

        # refresh canvas
        self.canvas.draw()


if __name__ == '__main__':

    app = QtGui.QApplication(sys.argv)
    main = MainWindow()
    main.show()
    sys.exit(app.exec_())
