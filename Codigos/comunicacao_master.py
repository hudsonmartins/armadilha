# -*- coding: utf-8 -*-
import serial, time, datetime, sys, os, json, config_xbee, gerenciador_pacotes, insert_data	

BAUDRATE, SERIALPORT, ADDRESS = config_xbee.get_xbee_configuration()
ser = serial.Serial(SERIALPORT, BAUDRATE,timeout = 0.5)
#xbee = XBee(ser)
ser.close()
ser.open()
os.chdir(os.path.dirname(os.path.abspath(sys.argv[0])))
global delInicio, delCabecalho, delFim, funcao
delInicio = '&' #Delimitador para inicio do pacote
delCabecalho = '~' #Delimitador do cabeçalho
delFim = '@' #Delimitador para fim do pacote
funcao = {'hello':'0', 'dados':'1', 'ack':'2'} #byte de funcao do cabecalho
NAK = '000' #Dados para o pacote NAK (Not Acknowledge)
ACK = '111' #Dados para o pacote ACK (Acknowledge)
BROADCAST = '11111111'
MAX_PACOTES = 20
lista_pacotes = []

def calc_checksum(s):
        '''
        Calculates checksum for sending commands to the ELKM1.
        Sums the ASCII character values mod256 and takes
        the lower byte of the two's complement of that value.
        '''
        return '%2X' % (-(sum(ord(c) for c in s) % 256) & 0xFF)

def codifica_dados(num_moscas, id_ponto):
	'''
	Coloca os dados da armadilha em JSON
	'''
	return json.dumps({'data': time.strftime("%Y-%m-%d"), 'hora': time.strftime("%H:%M:%S"), 'moscas': num_moscas, 'endereco': id_ponto}, separators=(',', ': '), sort_keys=True)


def salva_dados(dado, ponto):
	'''
	Salva dados recebidos em um txt
	'''
	arquivo = 'data/dados_serial.txt'
	#grava entradas no txt
	if os.path.isfile(arquivo):
		arquivo = open(arquivo, 'a+')
	else:
		arquivo = open(arquivo, 'w+')
	json_data = codifica_dados(dado, ponto)
	arquivo.write(json_data+'\n')
	arquivo.close()
	insert_data.inserir(json_data)

def envia_hello():
	'''
	Envia o pacote de hello, contendo o endereco do master no cabecalho e a data nos dados
	'''
	print "Enviando Hello..."
	data = time.strftime("%Y%m%d\ %H%M")
	gerenciador_pacotes.enviar_dados(data, 'hello', BROADCAST, ADDRESS)
	"""start=time.time()
	while(not recebe_ack()):
		if (time.time() > start+2): #envia novamente a cada 2 segundos se nao receber pacote coerente
			gerenciador_pacotes.enviar_dados(data, 'hello', BROADCAST, ADDRESS)
			print "Enviando Hello..."
			start=time.time()	
	"""
	


def salva_id_pacote(id_pacote):
	'''
		Salva o ID do pacote em uma lista. Caso a lista tenha atingido o tamanho maximo remove o ultimo ID
	'''
	if(len(lista_pacotes) == MAX_PACOTES):
		del(lista_pacotes[MAX_PACOTES-1])
		lista_pacotes.append(id_pacote)
		#print "Lista de pacotes: ", lista_pacotes
	else:
		lista_pacotes.append(id_pacote)
		#print "Lista de pacotes: ", lista_pacotes


def comunica():
	'''
	Ouve o que chega na serial e envia ack se houver integridade nos dados
	'''
	envia_hello() #Envia o pacote de HELLO para todos os pontos
	dados = ''
	cabecalho = ''
	leitura = ''
	
	inicioPct = False #Flag que indica que o pacote comecou a ser lido
	fimPct = False #Flag que indica que o pacote terminou de ser lido
	lendoCabecalho = False #Flag que indica que esta lendo o cabecalho
	print '...'
	while(True):
	    cabecalho, dados = gerenciador_pacotes.recebe_pacote()

            if(cabecalho):
			bt_funcao, end_destino, end_remetente, bt_chksum, id_pacote = gerenciador_pacotes.campos_cabecalho(cabecalho)

	  		#se o pacote recebido for direcionado para este ponto
			if(end_destino == ADDRESS):
				#se for um pacote de dados
				if (bt_funcao == funcao.get('dados')):		
					
					#calcula checksum para verificar a integridade dos dados
					print 'Chksum Lido '+bt_chksum
					checksum = gerenciador_pacotes.calc_checksum(dados)
					print 'Chksum Calculado '+checksum

					#Envia ACK ou NAK para indicar ao ponto remetente que recebeu o pacote com sucesso ou sem 						sucesso respectivamente
					if(dados != ''):
						print dados

						if(str(checksum) == bt_chksum):
							print 'Dados sem ruido!\n'
							time.sleep(0.0001)
							gerenciador_pacotes.enviar_dados(ACK, 'ack', end_remetente, ADDRESS)
							print 'ACK enviado\n'

							#Se ainda nao enviou este pacote para a rede
							if(id_pacote not in lista_pacotes):
								salva_dados(dados, end_remetente) #salva dados que recebeu	
								salva_id_pacote(id_pacote)	#guarda o id do pacote
						else:
							print 'Dados com ruido!\n'
							time.sleep(0.0001)
							gerenciador_pacotes.enviar_dados(NAK, 'ack', end_remetente, ADDRESS)
							print 'NAK enviado\n'

					else:
						print 'Nao recebeu dados\n'
						time.sleep(0.0001)
						gerenciador_pacotes.enviar_dados(NAK, 'ack', end_remetente, ADDRESS)
						print 'NAK enviado\n'

            dados=''
	    cabecalho = ''
	    inicioPct = False
	    fimPct = False

comunica()
ser.close()

