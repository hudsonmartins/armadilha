import mysql.connector
from mysql.connector import errorcode, MySQLConnection, Error
import python_mysql_dbconfig

def create_database(name):
    python_mysql_dbconfig.set_database(name)
    try:
        db_config = python_mysql_dbconfig.read_db_config()
        conn = mysql.connector.connect(host = db_config.get('host'), user = db_config.get('user'), password = db_config.get('password'))

        cursor = conn.cursor()
        cursor.execute("CREATE DATABASE "+name)       

        conn.commit()
	cursor.close()
        conn.close()
    except Error as error:
        return error

def create_table():

	sql = "CREATE TABLE fazenda (id INT NOT NULL AUTO_INCREMENT, endereco VARCHAR(150) NOT NULL, moscas INT NOT NULL, data DATE NOT NULL, hora TIME NOT NULL, PRIMARY KEY (id))"
	try:
		db_config = python_mysql_dbconfig.read_db_config()
		conn = mysql.connector.connect(**db_config)
		cursor = conn.cursor()
		cursor.execute(sql)       

		conn.commit()
		cursor.close()
		conn.close()
	except Error as error:
	        return error

