 #-*- coding: utf-8 -*-
import numpy as np
import RPi.GPIO as GPIO
import datetime, cv2, os, time, picamera, serial, sys, os.path
from time import sleep

global num_fotos
num_fotos = 5

def get_moscas():
	'''
	Salva imagens tiradas da armadilha com a contagem de moscas
	Retorna um vetor com num_fotos contagem de moscas
	'''	
	global num_fotos
	#Inicia camera
	camera=picamera.PiCamera()
	
	num_diretorio = 0
	diretorio ='/home/pi/Desktop/Fotos/folder%s'%num_diretorio

	while(os.path.isdir(diretorio)):
		num_diretorio += 1
		diretorio = '/home/pi/Desktop/Fotos/folder%s'%num_diretorio
		#print x
	
	os.system('sudo mkdir '+diretorio) 
	print "Criou pasta "+diretorio

	contagem_moscas = []
	for i in range(num_fotos):
		#tira a foto e salva	
		print "Tirando fotos..."
		path_foto = diretorio + '/image%s.jpg'%i
		camera.capture(path_foto)	 
	
		#abre a foto para converter a cor
		cor = cv2.imread(path_foto)
		arr = np.array(cor)
		arr[:,:,0] *=2	
		arr[:,:,2] /=4  
		arr[:,:,1] /=4
		cap = cv2.cvtColor(arr, 6)
		cap[:,:] *= 4
	
		#executa a rede neural para contagem
		cascade = cv2.CascadeClassifier('cascade/cascade.xml')
		#salva o vetor de moscas
		flies = cascade.detectMultiScale(cap, minNeighbors = 1, minSize=(0,0),maxSize=(25,25))
		print "Encontrei ", len(flies), " moscas"
	
		#Desenha retangulos nas moscas
		for (x,y,w,h) in flies:
			cv2.rectangle(cap, (x,y),(x+w,y+h),(255,255,0),2)
	 
		#Salva a imagem com os retangulos
		cv2.imwrite(path_foto+'/image_flies%s.jpg'%i, cap)
		contagem_moscas.append(len(flies))
	return contagem_moscas
