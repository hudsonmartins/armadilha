from mysql.connector import MySQLConnection, Error
from python_mysql_dbconfig import read_db_config
from database_manager import create_database, create_table
import json

def inserir(dados):
    """
	Cria o banco de dados armadilha e a tabela fazenda
	Recebe os dados em json e salva no banco
    """
    dado = json.loads(dados)
    create_database('armadilha')
    create_table()
    salva_bd(dado['endereco'], dado['moscas'], dado['data'], dado['hora'])
    
def salva_bd(endereco, moscas, data, hora):
    """
	Salva os dados no banco de dados
    """	
    query = "INSERT INTO fazenda(endereco, moscas, data, hora) " \
		    "VALUES(%s,%s,%s,%s)"
    args = (endereco, moscas, data, hora)
 
    try:
        db_config = read_db_config()
        conn = MySQLConnection(**db_config)
        cursor = conn.cursor()
        cursor.execute(query, args)
 
        if cursor.lastrowid:
            print('last insert id', cursor.lastrowid)
        else:
            print('last insert id not found')
 
        conn.commit()
	cursor.close()
        conn.close()
    except Error as error:
        print(error)
 
