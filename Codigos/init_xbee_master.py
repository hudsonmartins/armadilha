 #-*- coding: utf-8 -*-
import comando_at, config_xbee, time, serial

delInicio = '&' #Delimitador para inicio do pacote
delCabecalho = '~' #Delimitador do cabeçalho
delFim = '@' #Delimitador para fim do pacote
NAK = '000' #Dados para o pacote NAK (Not Acknowledge)
ACK = '111' #Dados para o pacote ACK (Acknowledge)
funcao = {'hello':'0', 'dados':'1'}
BAUDRATE, SERIALPORT, ADDRESS = config_xbee.get_xbee_configuration()

ser = serial.Serial(SERIALPORT, BAUDRATE, timeout = 0.5)
#xbee = XBee(ser)
ser.close()
ser.open()

def inicia_xbee():
	#comando_at.inicia()
	endereco = comando_at.escreve('atsl') #envia comando at para pegar a parte baixa do endereco mac do xbee
	print "Endereco XBEE: ", endereco
	config_xbee.set_xbee_address(endereco)
	comando_at.escreve('atcn')

inicia_xbee()
