import config_xbee, serial, time

BAUDRATE, SERIALPORT, ADDRESS = config_xbee.get_xbee_configuration()

ser = serial.Serial(SERIALPORT, BAUDRATE,timeout = 0.5)
ser.close()
ser.open()

def inicia():
	'''
		Tenta comunicar com a xbee via AT
	'''	
	tentativas = 0
	print "Conectando AT..."
	ser.write('+++')
	time.sleep(1)
	resposta = ser.read(3)

	#print repr(resposta)
	while not 'OK' in resposta:
		tentativas += 1
		if tentativas > 5:
			print "Verifique sua conexao e tente novamente!"
			break

		print "Nao foi possivel conectar, tentando novamente...", tentativas
		ser.write('+++')
		time.sleep(1)
		resposta = ser.read(3)
		
	if 'OK' in resposta:
		print "Conectado"
		ser.write('\r')
		time.sleep(0.1)

def escreve(comando):
	'''
		param: string comando (comando que sera enviado na serial)
		retur: resposta do comando
	'''	
	time.sleep(0.1)
	ser.write('at\r')
	resposta = ser.read(5)

	if not 'OK' in resposta:
		inicia()
	
	ser.write(comando+'\r')
	resposta = ser.read(20)
	return resposta

