from ConfigParser import RawConfigParser
import os, sys

'''
Utiliza config parser para ler e alterar o arquivo config.ini
'''

#filename = os.chdir(os.path.dirname(os.path.abspath(sys.argv[0])))
os.chdir(os.path.dirname(os.path.abspath(sys.argv[0])))
#print filename
filename = 'utils/config.ini'
section_xbee = 'xbee'
section_master = 'master'

def get_xbee_configuration():
	'''
		Retorna os parametros da secao xbee
	'''
	parser = RawConfigParser()
	parser.read(filename)

	if parser.has_section(section_xbee):
		baudrate = parser.get(section_xbee, 'baudrate')
		serial_port = parser.get(section_xbee, 'serial_port')
		address=parser.get(section_xbee, 'address')
	else:
		raise Exception('{0} not found in the {1} file'.format(section_xbee, filename))

	return baudrate, serial_port, address

def set_xbee_address(address):
	'''
		Seta o endereco do xbee no arquivo
	'''
	parser = RawConfigParser()
	parser.read(os.path.abspath(filename))
	
	if parser.has_section(section_xbee):
		parser.set(section_xbee, 'address', address)
		with open(filename, 'wb') as configfile:
			parser.write(configfile)
			return True

def set_master_address(address):
	'''
		Seta o endereco do master no arquivo
	'''
	parser = RawConfigParser()
	parser.read(os.path.abspath(filename))
	
	if parser.has_section(section_master):
		parser.set(section_master, 'address', address)
		with open(filename, 'wb') as configfile:
			parser.write(configfile)
			return True

def get_master_address():
	parser = RawConfigParser()
	parser.read(filename)

	if parser.has_section(section_master):
		address=parser.get(section_master, 'address')
	else:
		raise Exception('{0} not found in the {1} file'.format(section_master, filename))

	return address
