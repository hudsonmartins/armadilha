from mysql.connector import MySQLConnection, Error
from python_mysql_dbconfig import read_db_config
import json

def insert_data(endereco, num_moscas, data, hora):
    query = "INSERT INTO fazenda(endereco, moscas, data, hora) " \
            "VALUES(%s,%s,%s,%s)"
    args = (endereco, num_moscas, data, hora)
 
    try:
        db_config = read_db_config()
        conn = MySQLConnection(**db_config)
 
        cursor = conn.cursor()
        cursor.execute(query, args)
 
        if cursor.lastrowid:
            print('last insert id', cursor.lastrowid)
        else:
            print('last insert id not found')
 
        conn.commit()
    except Error as error:
        print(error)
 
    finally:
        cursor.close()
        conn.close()
 
def main():
   file = open('../data/dados_serial.txt')
   dados = file.readlines()
   dado = json.loads(len(dados)-1)
   insert_data(dado['endereco'], dado['moscas'], dado['data'], dado['hora'])
 
if __name__ == '__main__':
    main()
