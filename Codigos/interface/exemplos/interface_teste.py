#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
from PyQt4.QtGui import *
from PyQt4.QtCore import *
import query_data
dados=None

class Interface(QWidget):
    
	def __init__(self):
		super(Interface, self).__init__()    
		self.selected_book = None
		self.initUI()

	def initUI(self):
		dados = query_data.query_all_data()		
		self.setGeometry(300, 300, 400, 400)

		self.listWidget = QListWidget(self)
		self.listWidget.resize(300,200)	
		self.listWidget.itemSelectionChanged.connect(self.printItemText)
		for row in dados:
            		self.listWidget.addItem(row[0])
		
		self.listWidget.move(50,50)
		#print self.listWidget.currentItem()
		#----------- botão -----------------
		self.button = QPushButton('Ver detalhes', self)
		self.button.clicked.connect(self.bt_pressed)		
		self.button.move(150, 300)

		self.listWidget.show()
		self.show()
	
	def printItemText(self):
		self.selected_book = self.listWidget.currentItem().text()

	def bt_pressed(self):
		query_data.select_book(self.selected_book)		
		dialog = QDialog()
		dialog.setGeometry(300, 300, 400, 400)
		info = query_data.select_book(self.selected_book)

		labelID = QLabel("id: "+str(info[0][0]), dialog)
		labelID.move(50,80)

		labelTitle = QLabel("Title: "+str(info[0][1]), dialog)
		labelTitle.move(50,100)

		labelAuthor = QLabel("Author: "+str(info[0][3])+" "+str(info[0][4]), dialog)
		labelAuthor.move(50,120)		

		labelISBN = QLabel("ISBN: "+str(info[0][2]), dialog)
		labelISBN.move(50,140)		

		dialog.exec_()




def main():
    app = QApplication(sys.argv)
    ex = Interface()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()  
