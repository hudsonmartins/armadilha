import sys
from PyQt4 import QtGui
import numpy as np
import matplotlib.pyplot as plt
import query_data
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

import random

class Window(QtGui.QDialog):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)

        # a figure instance to plot on
        self.figure = Figure()

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)

        # Just some button connected to `plot` method
        self.button = QtGui.QPushButton('Plot')
        self.button.clicked.connect(self.plot)

        # set the layout
        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)
        layout.addWidget(self.button)
        self.setLayout(layout)

    def onpick(self, event):
        ind = event.ind
        print('onpick3 scatter:', ind, np.take(x, ind), np.take(y, ind))


    def plot(self):

	dados = query_data.query_all_data()

	N = len(dados)

	x = np.random.rand(N)
	y = np.random.rand(N)

	num_moscas = []
	maximo_moscas = 0.0

	for n in range(N):
		num_moscas.append(dados[n][3])
		if(dados[n][3] > maximo_moscas):
			maximo_moscas = dados[n][3]

	area = np.pi * (15 * 2)**2


        ax = self.figure.add_subplot(111)
	
	for i in range(N):
		if(num_moscas[i]>0):
			ax.scatter(x[i], y[i], s=area, c='r', alpha=(num_moscas[i]*1.0/maximo_moscas))
			#plt.scatter(x[i], y[i], s=area, c='r', alpha=(num_moscas[i]*1.0/maximo_moscas))
		else:
			ax.scatter(x[i], y[i], s=area, c='g', alpha=0.5)
			#plt.scatter(x[i], y[i], s=area, c='g', alpha=0.5)

	#plt.show()
        # refresh canvas
	self.figure.canvas.mpl_connect('pick_event', self.onpick)
        self.canvas.draw()

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)

    main = Window()
    main.show()

    sys.exit(app.exec_())
