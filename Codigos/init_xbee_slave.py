 #-*- coding: utf-8 -*-
import comando_at, config_xbee, time, serial, os, gerenciador_pacotes, comunicacao_slave


funcao = {'hello':'0', 'dados':'1'}

def inicia_xbee():
	#comando_at.inicia()
	endereco = comando_at.escreve('atsl') #envia comando at para pegar a parte baixa do endereco mac do xbee
	print "Endereco XBEE: ", endereco
	config_xbee.set_xbee_address(endereco)
	comando_at.escreve('atcn')

def espera_master():
	cabecalho, dados = gerenciador_pacotes.recebe_pacote()	
        
	if(cabecalho):	
		bt_funcao, end_destino, end_remetente, checksum, id_pacote = gerenciador_pacotes.campos_cabecalho(cabecalho)  
		if (bt_funcao == funcao.get('hello')):
			print "Pacote de Hello"
			config_xbee.set_master_address(end_remetente)
			print dados
			os.system("sudo date  +' %Y%m%d %H%m ' --set="+ dados)
			print "Enviando para rede mesh: hello"
			#Envia o mesmo pacote para a rede
			gerenciador_pacotes.enviar_dados(dados, 'hello', end_destino, end_remetente) 
			return True
		else:
			return False

inicia_xbee()

print "..."

while (not espera_master()):
	continue

comunicacao_slave.comunica()	
