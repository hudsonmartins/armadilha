from mysql.connector import MySQLConnection, Error
from python_mysql_dbconfig import read_db_config
 
def query_all_data():
	try:
		dbconfig = read_db_config()
		conn = MySQLConnection(**dbconfig)
		cursor = conn.cursor()
		cursor.execute("SELECT * FROM fazenda")
		rows = cursor.fetchall()

		#print('Total Row(s):', cursor.rowcount)
		#for row in rows:
		 #   print(row)
	except Error as e:
		print(e)
 
	finally:
		cursor.close()
		conn.close()
	return rows	

def select_armadilha(id):
    try:
        dbconfig = read_db_config()
        conn = MySQLConnection(**dbconfig)
        cursor = conn.cursor()
        query = ('SELECT * FROM fazenda WHERE id = %s')
        #print title
        cursor.execute(query, (str(id),))
        data = cursor.fetchall()
        print data
            #print('Total Row(s):', cursor.rowcount)
            #for row in rows:
            #print(row)
    except Error as e:
        print(e)
    finally:
        cursor.close()
        conn.close()
    return data

"""
def select_book(title):
    try:
        dbconfig = read_db_config()
        conn = MySQLConnection(**dbconfig)
        cursor = conn.cursor()
        query = ('SELECT books.id, books.title, books.isbn, authors.first_name, authors.last_name ' 
		'FROM books '
		'INNER JOIN book_author '
		'ON books.id = book_author.book_id '
		'INNER JOIN authors '
		'ON authors.id = book_author.author_id '
		'WHERE title = %s')
        #print title
        cursor.execute(query, (str(title),))
        data = cursor.fetchall()
        print data
            #print('Total Row(s):', cursor.rowcount)
            #for row in rows:
            #print(row)
    except Error as e:
        print(e)
    finally:
        cursor.close()
        conn.close()
    return data
"""
