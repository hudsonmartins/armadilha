 #-*- coding: utf-8 -*-
import numpy as np
import datetime, os, time, serial, sys, os.path, config_xbee
"""
----------------------------------------Definicoes do protocolo de comunicacao versao 3.0------------------------------------------

+--------------------------------------------------------------------------------------------------------+
|                       		       Pacote de dados                                           |
+-----------------+------------------------+--------------------------+-----------+--------------+-------+
|                 |                        |                          |           |              |       |
|Funcao do pacote |Endereco MAC de destino |Endereco MAC do remetente | Checksum  |     ID       | Dados |
|     1 Byte      |       8 Bytes          |        8 Bytes           |  2 Bytes  |   20 Bytes   |       |
|                 |                        |                          |           |              |       |
+-----------------+------------------------+--------------------------+-----------+--------------+-------+

"""
delInicio = '&' #Delimitador para inicio do pacote
delCabecalho = '~' #Delimitador do cabeçalho
delFim = '@' #Delimitador para fim do pacote
inicioPct = False #Flag que indica que o pacote comecou a ser lido
fimPct = False #Flag que indica que o pacote terminou de ser lido
lendoCabecalho = False #Flag que indica que esta lendo o cabecalho
funcao = {'hello':'0', 'dados':'1', 'ack':'2'}

BAUDRATE, SERIALPORT, ADDRESS = config_xbee.get_xbee_configuration()

ser = serial.Serial(SERIALPORT, BAUDRATE, timeout = 0.5)
#xbee = XBee(ser)
ser.close()
ser.open()
#Serial iniciada
print '...'

def calc_checksum(s):
        """
        Calculates checksum for sending commands to the ELKM1.
        Sums the ASCII character values mod256 and takes
        the lower byte of the two's complement of that value.
        """
	return '%2X' % (-(sum(ord(c) for c in s) % 256) & 0xFF)

def calcula_id(end_remetente):
	"""
		Calcula o ID do pacote baseado no endereco MAC do ponto remetente e a data e hora atual
	"""
	return end_remetente + time.strftime("%d%m%y%H%M%S")



def enviar_dados(dado, bt_funcao, end_destino, end_remetente):
        """   
        Envia o pacote com os delimitadores e cabeçalho
        """

        ser.write(delInicio) #inicia pacote
	
	#------------cabecalho-----------------
	if bt_funcao is not None:
	        ser.write(funcao.get(bt_funcao)) #Byte de funcao

	if end_destino is not None:
		ser.write(str(end_destino))	#Byte endereco MAC destino
	
	if end_remetente is not None:
		ser.write(str(end_remetente))	#Byte endereco MAC remetente
	
        ser.write(calc_checksum(dado))		#Checksum
	id_pct = calcula_id(end_remetente)
	ser.write(id_pct)	#ID

        ser.write(delCabecalho)

	#------------dados---------------------
        ser.write(dado)

        ser.write(delFim)

def recebe_pacote():
    """
       Recebe pacotes enviados pelo master
    """
    pacote=''  # reseta os dados do metódo            
    dados = ''
    cabecalho = ''
    inicioPct = False # reseta os dados do metódo 
    fimPct = False # reseta os dados do metódo 
    lendoCabecalho = False
    #print('entrou1')
    start=time.time() # inicia variavél de controle

    #Executa loop ate o fim do pacote
    while(not fimPct):
        
        if (time.time()>start+ .2):#Retorna falso se não houver nada na serial, sem isso o ser.inWaiting()>0 nunca será verdadeiro, logo o metódo nunca retorna
		
                return cabecalho, dados
	
	#Enquanto houver dados na serial
        while(ser.inWaiting() > 0):
           
           try:
                leitura = ser.read()
                #print leitura              
		if(inicioPct and not (fimPct)): #se esta lendo o pacote 
			if(lendoCabecalho):         #se esta no cabecalho
				if(leitura != delCabecalho):
					cabecalho += leitura  #guarda o cabecalho
			elif(not lendoCabecalho):
				if(leitura != delFim):
					dados += leitura   #guarda os dados
                                 
                if(leitura == delInicio): #se a leitura for igual ao delimitador de inicio
			inicioPct = True                   
			lendoCabecalho = True
		        #print 'Inicio'
                    
		elif(leitura == delCabecalho):
			lendoCabecalho = False

                elif(leitura == delFim): #se a leitura for igual ao delimitador de final
			fimPct = True
                        #print 'Fim do pct' 
               
           except KeyboardInterrupt:
               break
    #print "Retornando ", cabecalho, " e ", dados          
    return cabecalho, dados

def campos_cabecalho(cabecalho):
	func = list(cabecalho)[0] 	  #Primeiro Byte
	end_destino = cabecalho[1:9]	  #Bytes de 1 a 8	
	end_remetente = cabecalho[9:17]	  #Bytes de 9 a 16 
	checksum = cabecalho[17:19]       #Bytes de 17 a 18
	id_pct = cabecalho[19:39]         #Bytes de 19 a 38
	return func, end_destino, end_remetente, checksum, id_pct
